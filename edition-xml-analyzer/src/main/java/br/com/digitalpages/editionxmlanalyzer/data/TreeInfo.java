package br.com.digitalpages.editionxmlanalyzer.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rodrigo.botelho on 19/03/2017.
 */
public class TreeInfo {
    private String name;
    private List<TreeInfo> children;

    public TreeInfo(String name) {
        this.name = name;
        children = new ArrayList<TreeInfo>();
    }

    public String getName() {
        return name;
    }

    public void setChildren(List<TreeInfo> children) {
        this.children = children;
    }

    public List<TreeInfo> getChildren() {
        return children;
    }
}
