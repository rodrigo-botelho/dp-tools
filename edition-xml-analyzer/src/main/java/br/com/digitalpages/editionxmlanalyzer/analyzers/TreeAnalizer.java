package br.com.digitalpages.editionxmlanalyzer.analyzers;

import br.com.digitalpages.editionxmlanalyzer.data.TreeInfo;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rodrigo.botelho on 19/03/2017.
 */
public class TreeAnalizer {
    private String filePath;
    private TreeInfo info;

    public TreeAnalizer(String filePath) {
        this.filePath = filePath;
    }

    public void analyze() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(true);
        factory.setIgnoringElementContentWhitespace(true);
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            File file = new File(filePath);
            Document doc = builder.parse(file);
            doc.getDocumentElement().normalize();

            List<TreeInfo> children = new ArrayList<TreeInfo>();
            NodeList list = doc.getChildNodes();
            nodeRecursive(0, children, list);

            info = new TreeInfo(doc.getDocumentElement().getNodeName());
            info.setChildren(children);

            print(0, info.getName(), info.getChildren());
        } catch (ParserConfigurationException e) {
        } catch (SAXException e) {
        } catch (IOException e) {
        }
    }

    private void print(int level, String name, List<TreeInfo> children) {
        printSpaces(level * 2);
        System.out.println(name);
        for (TreeInfo info : children) {
            print(level + 1, info.getName(), info.getChildren());
        }
    }

    private void printSpaces(int length) {
        for (int i = 0; i < length; i++) {
            System.out.print(" ");
        }
    }

    private void nodeRecursive(int level, List<TreeInfo> children, NodeList list) {
        int length = list.getLength();
        Node node;
        String nodeName;
        boolean nodeFound;
        List<TreeInfo> currentChildren;
        TreeInfo currentInfo = null;

        for (int i = 0; i < length; i++) {
            node = list.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                nodeName = node.getNodeName();
                nodeFound = false;
                for (TreeInfo innerInfo : children) {
                    if (innerInfo.getName().equals(nodeName)) {
                        nodeFound = true;
                        currentInfo = innerInfo;
                        break;
                    }
                }
                if (!nodeFound) {
                    currentInfo = new TreeInfo(nodeName);
                    children.add(currentInfo);
                }

                currentChildren = currentInfo.getChildren();
                nodeRecursive(level + 1, currentChildren, node.getChildNodes());
            }
        }
    }
}
