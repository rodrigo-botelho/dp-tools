package br.com.digitalpages.editionxmlanalyzer.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by rodrigo.botelho on 19/03/2017.
 */
public class TagAttributesInfo {
    private HashMap<String, List<String>> attributesValues;

    public TagAttributesInfo() {
        attributesValues = new HashMap<String, List<String>>();
    }

    public void putTagValue(String tag, String value) {
        List<String> values;
        if (attributesValues.containsKey(tag)) {
            values = attributesValues.get(tag);
            if (!values.contains(value)) {
                values.add(value);
            }
        } else {
            values = new ArrayList<String>();
            values.add(value);
            attributesValues.put(tag, values);
        }
    }

    public HashMap<String, List<String>> getAttributesValues() {
        return attributesValues;
    }
}
