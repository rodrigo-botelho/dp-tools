package br.com.digitalpages.editionxmlanalyzer.analyzers;

import br.com.digitalpages.editionxmlanalyzer.data.TagAttributesInfo;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by rodrigo.botelho on 19/03/2017.
 */
public class AttributesAnalyzer {
    private String filePath;
    private HashMap<String, TagAttributesInfo> tagsInfo;

    public AttributesAnalyzer(String filePath) {
        this.filePath = filePath;
        tagsInfo = new HashMap<String, TagAttributesInfo>();
    }

    public void analyze() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(true);
        factory.setIgnoringElementContentWhitespace(true);
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            File file = new File(filePath);
            Document doc = builder.parse(file);
            doc.getDocumentElement().normalize();

            NodeList list = doc.getChildNodes();
            nodeRecursive(list);
            print();
        } catch (ParserConfigurationException e) {
        } catch (SAXException e) {
        } catch (IOException e) {
        }
    }

    private void print() {
        Set<Map.Entry<String, TagAttributesInfo>> entrySet = tagsInfo.entrySet();
        Set<Map.Entry<String, List<String>>> innerEntrySet;
        List<String> list;
        for (Map.Entry<String, TagAttributesInfo> entry : entrySet) {
            System.out.println("TAG: " + entry.getKey());
            innerEntrySet = entry.getValue().getAttributesValues().entrySet();
            System.out.println("ATTRs:");
            for (Map.Entry<String, List<String>> innerEntry : innerEntrySet) {
                list = innerEntry.getValue();
                System.out.print(innerEntry.getKey() + ": ");
                for (String value : list) {
                    System.out.print(value + " ");
                }
                System.out.println();
            }
            System.out.println("-------");
        }
    }

    private void nodeRecursive(NodeList list) {
        int length = list.getLength();
        Node node;
        String nodeName;
        for (int i = 0; i < length; i++) {
            node = list.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                nodeName = node.getNodeName();
                //System.out.println(nodeName);

                if (!tagsInfo.containsKey(nodeName)) {
                    tagsInfo.put(nodeName, new TagAttributesInfo());
                }
                nodeAttributes(nodeName, node.getAttributes());
                nodeRecursive(node.getChildNodes());
            }
        }
    }

    private void nodeAttributes(String nodeName, NamedNodeMap attr) {
        if (attr != null) {
            int length = attr.getLength();
            Node node;
            TagAttributesInfo tagAttributesInfo = tagsInfo.get(nodeName);
            for (int i = 0; i < length; i++) {
                node = attr.item(i);
                tagAttributesInfo.putTagValue(node.getNodeName(), node.getNodeValue());
                //System.out.println(node.getNodeName() + " - " + node.getNodeValue());
            }
        }
    }
}
