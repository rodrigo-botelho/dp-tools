package br.com.digitalpages.editionxmlanalyzer;

import br.com.digitalpages.editionxmlanalyzer.analyzers.AttributesAnalyzer;
import br.com.digitalpages.editionxmlanalyzer.analyzers.TreeAnalizer;

import java.io.File;

/**
 * Created by rodrigo.botelho on 19/03/2017.
 */
public class Main {
    public static void main(String[] args) {
        if (args.length != 1) {
            String programName = new File(Main.class.getProtectionDomain()
                    .getCodeSource()
                    .getLocation()
                    .getPath())
                    .getName();
            System.out.println("Usage: " + programName + " edition-xml-name");
            return;
        }

        String path = args[0];
        new AttributesAnalyzer(path).analyze();
        new TreeAnalizer(path).analyze();
    }
}
