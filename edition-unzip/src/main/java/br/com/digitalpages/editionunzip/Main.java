package br.com.digitalpages.editionunzip;

import br.com.digitalpages.editionunzip.io.UnzipEdition;

import java.io.File;

/**
 * Created by rodrigo.botelho on 17/03/2017.
 */
public class Main {
    public static void main(String[] args) {
        if (args.length != 2) {
            String programName = new File(Main.class.getProtectionDomain()
                    .getCodeSource()
                    .getLocation()
                    .getPath())
                    .getName();
            System.out.println("Usage: " + programName + " edition-dir output-dir");
            return;
        }

        String editionDir = args[0];
        String outputDir = args[1];
        UnzipEdition edition = new UnzipEdition(editionDir, outputDir);
        edition.unzip();
        String error = edition.getError();

        System.out.println("Completo.");
        if (error != null && error.length() > 0) {
            System.out.println("Alguns erros durante o processo: ");
            System.out.println(error);
        }
    }
}
