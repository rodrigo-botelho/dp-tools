package br.com.digitalpages.editionunzip.io;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

import java.io.File;

/**
 * Created by rodrigo.botelho on 17/03/2017.
 */
public class UnzipEdition {
    private String editionDir;
    private String outputDir;
    private StringBuilder error;

    public UnzipEdition(String editionDir, String outputDir) {
        this.editionDir = editionDir;
        this.outputDir = outputDir;
        error = new StringBuilder();
    }

    public void unzip() {
        error.setLength(0);

        File sourceDir = new File(editionDir);
        if (sourceDir.exists()) {
            File[] files = sourceDir.listFiles();
            if (files != null) {
                for (File file : files) {
                    unzip(file.getAbsolutePath(), new File(outputDir).getAbsolutePath());
                }
            }
        } else {
            error.append(editionDir);
            error.append(" não existe.");
        }
    }

    public String getError() {
        return error.toString();
    }

    private boolean unzip(String source, String destination) {
        //String password = "password";
        boolean success = false;

        try {
            ZipFile zipFile = new ZipFile(source);
            if (zipFile.isEncrypted()) {
                error.append(source);
                error.append(" está protegido por senha.\n");
            } else {
                zipFile.extractAll(destination);
                success = true;
            }
        } catch (ZipException e) {
            error.append(source);
            error.append(": ");
            error.append(e.getLocalizedMessage());
            error.append("\n");
        }

        return success;
    }
}
