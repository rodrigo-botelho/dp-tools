package br.com.digitalpages.editiondownload.data;

/**
 * Created by rodrigo.botelho on 17/03/2017.
 */
public class PageSet {
    private String richPack;

    public PageSet(String richPack) {
        this.richPack = richPack;
    }

    public String getRichPack() {
        return richPack;
    }
}
