package br.com.digitalpages.editiondownload.data;

import java.util.List;

/**
 * Created by rodrigo.botelho on 17/03/2017.
 */
public class Section {
    private String id;
    private List<PageSet> pageSets;

    public Section(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public List<PageSet> getPageSets() {
        return pageSets;
    }

    public void setPageSets(List<PageSet> pageSets) {
        this.pageSets = pageSets;
    }
}
