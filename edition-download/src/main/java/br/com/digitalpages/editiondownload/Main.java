package br.com.digitalpages.editiondownload;

import br.com.digitalpages.editiondownload.data.Edition;
import br.com.digitalpages.editiondownload.parser.EditionParser;
import br.com.digitalpages.editiondownload.services.DownloadEdition;
import br.com.digitalpages.editiondownload.services.DownloadFile;

import java.io.File;

/**
 * Created by rodrigo.botelho on 17/03/2017.
 */
public class Main {
    public static void main(String[] args) {
        if (args.length != 3) {
            String programName = new File(Main.class.getProtectionDomain()
                    .getCodeSource()
                    .getLocation()
                    .getPath())
                    .getName();
            System.out.println("Usage: " + programName + " edition-xml-url output-dir edition-xml-name");
            System.out.println("Example: " + programName + " https://sunflower2.digitalpages.com.br/richReader/getEditionXml?editionId=27946 edition/27946/ edition.27946.xml");
            return;
        }

        String url = args[0];
        String outputDir = args[1];
        String destination = outputDir + args[2];

        DownloadFile editionFile = new DownloadFile(url, destination);
        editionFile.download();

        if (editionFile.isSuccess()) {
            EditionParser parser = new EditionParser(destination);
            Edition edition = parser.parse();
            new DownloadEdition(edition, outputDir).download();
        } else {
            System.out.println(editionFile.getError());
        }
    }
}
