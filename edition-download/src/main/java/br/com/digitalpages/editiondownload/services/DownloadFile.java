package br.com.digitalpages.editiondownload.services;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created by rodrigo.botelho on 17/03/2017.
 */
public class DownloadFile {
    private String url;
    private String destination;
    private boolean success;
    private String error;

    public DownloadFile(String url, String destination) {
        this.url = url;
        this.destination = destination;
    }

    public void download() {
        success = false;
        try {
            File outputFile = new File(destination);
            FileUtils.copyURLToFile(new URL(url), outputFile);
            success = true;
        } catch (IOException e) {
            error = e.getLocalizedMessage();
        }
    }

    public boolean isSuccess() {
        return success;
    }

    public String getError() {
        return error;
    }
}
