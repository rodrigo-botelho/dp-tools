package br.com.digitalpages.editiondownload.data;

import java.util.List;

/**
 * Created by rodrigo.botelho on 17/03/2017.
 */
public class Edition {
    private String baseUri;
    private List<Section> sections;

    public Edition(String baseUri) {
        this.baseUri = baseUri;
    }

    public String getBaseUri() {
        return baseUri;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }
}
