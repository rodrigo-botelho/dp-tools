package br.com.digitalpages.editiondownload.services;

import br.com.digitalpages.editiondownload.data.Edition;
import br.com.digitalpages.editiondownload.data.PageSet;
import br.com.digitalpages.editiondownload.data.Section;

/**
 * Created by rodrigo.botelho on 17/03/2017.
 */
public class DownloadEdition {
    private Edition edition;
    private String outputDir;

    public DownloadEdition(Edition edition, String outputDir) {
        this.edition = edition;
        this.outputDir = outputDir;
    }

    public void download() {
        String baseUri = edition.getBaseUri();
        if (!baseUri.endsWith("/")) {
            baseUri += "/";
        }

        for (Section section : edition.getSections()) {
            for (PageSet pageSet : section.getPageSets()) {
                String rickPack = pageSet.getRichPack();
                if (rickPack.startsWith("/")) {
                    rickPack = rickPack.substring(1);
                }
                DownloadFile file = new DownloadFile(baseUri + rickPack, outputDir + rickPack);
                file.download();
                if (!file.isSuccess()) {
                    System.out.println(pageSet.getRichPack() + ": " + file.getError());
                }
            }
        }
    }
}
