package br.com.digitalpages.editiondownload.parser;

import br.com.digitalpages.editiondownload.data.Edition;
import br.com.digitalpages.editiondownload.data.PageSet;
import br.com.digitalpages.editiondownload.data.Section;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rodrigo.botelho on 17/03/2017.
 */
public class EditionParser {
    private String filePath;

    public EditionParser(String filePath) {
        this.filePath = filePath;
    }

    public Edition parse() {
        Edition edition = null;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(true);
        factory.setIgnoringElementContentWhitespace(true);
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            File file = new File(filePath);
            Document doc = builder.parse(file);
            doc.getDocumentElement().normalize();
            edition = new Edition(doc.getDocumentElement().getAttributeNode("baseUri").getValue());
            NodeList nodeList = doc.getElementsByTagName("section");
            List<Section> sections = new ArrayList<Section>();
            int length = nodeList.getLength();

            for (int i = 0; i < length; i++) {
                Node node = nodeList.item(i);
                Section section = null;
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    NodeList innerNodeList = node.getChildNodes();
                    int innerLength = innerNodeList.getLength();

                    if (innerLength > 0) {
                        section = new Section(node.getAttributes().getNamedItem("id").getNodeValue());
                        List<PageSet> pageSets = new ArrayList<PageSet>();
                        for (int j = 0; j < innerLength; j++) {
                            node = innerNodeList.item(j);
                            if (node.getNodeType() == Node.ELEMENT_NODE) {
                                pageSets.add(new PageSet(node.getAttributes().getNamedItem("richPack").getNodeValue()));
                            }
                        }
                        section.setPageSets(pageSets);
                    }
                }

                if (section != null) {
                    sections.add(section);
                }
            }
            edition.setSections(sections);
        } catch (ParserConfigurationException e) {
        } catch (SAXException e) {
        } catch (IOException e) {
        }
        return edition;
    }
}
