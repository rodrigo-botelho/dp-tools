package br.com.digitalpages.editionmergerickpackconfigs;

import br.com.digitalpages.editionmergerickpackconfigs.merge.EditionConfigMerger;

import java.io.File;

/**
 * Created by rodrigo.botelho on 17/03/2017.
 */
public class Main {
    public static void main(String[] args) {
        if (args.length != 2) {
            String programName = new File(Main.class.getProtectionDomain()
                    .getCodeSource()
                    .getLocation()
                    .getPath())
                    .getName();
            System.out.println("Usage: " + programName + " edition-dir output-dir");
            return;
        }

        String sourceDir = args[0];
        String outputFile = args[1];
        EditionConfigMerger edition = new EditionConfigMerger(sourceDir, outputFile);
        edition.mergeXMLs();

        String error = edition.getError();
        System.out.println("Completo.");

        if (error != null && error.length() > 0) {
            System.out.println("Alguns erros:");
            System.out.println(error);
        }
    }
}
