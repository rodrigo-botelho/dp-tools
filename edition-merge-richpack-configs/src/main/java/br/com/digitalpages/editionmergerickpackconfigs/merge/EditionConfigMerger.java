package br.com.digitalpages.editionmergerickpackconfigs.merge;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.nio.charset.Charset;

/**
 * Created by rodrigo.botelho on 17/03/2017.
 */
public class EditionConfigMerger {
    private String sourceDir;
    private String outputFile;
    private StringBuilder error;

    public EditionConfigMerger(String sourceDir, String outputFile) {
        this.sourceDir = sourceDir;
        this.outputFile = outputFile;
        error = new StringBuilder();
    }

    public void mergeXMLs() {
        File root = new File(sourceDir);
        File output = new File(outputFile);

        BufferedOutputStream bufferedOutputStream = null;
        FileOutputStream fileOutputStream = null;

        if (root.exists()) {
            try {
                fileOutputStream = new FileOutputStream(output);
                bufferedOutputStream = new BufferedOutputStream(fileOutputStream);

                String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<transformed>\r\n";
                bufferedOutputStream.write(xml.getBytes("UTF-8"));
                File[] files = root.listFiles();
                if (files != null) {
                    for (File file : files) {
                        mergeFileRecursive(file, bufferedOutputStream);
                    }
                }
                xml = "</transformed>\r\n";
                bufferedOutputStream.write(xml.getBytes("UTF-8"));
            } catch (FileNotFoundException e) {
                error.append(e.getLocalizedMessage());
                error.append("\n");
            } catch (IOException e) {
                error.append(e.getLocalizedMessage());
                error.append("\n");
            } finally {
                if (bufferedOutputStream != null) {
                    try {
                        bufferedOutputStream.close();
                    } catch (IOException e) {
                        error.append("Erro ao fechar bufferedOutputStream: ");
                        error.append(e.getLocalizedMessage());
                        error.append("\n");
                    }
                }

                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e) {
                        error.append("Erro ao fechar fileOutputStream: ");
                        error.append(e.getLocalizedMessage());
                        error.append("\n");
                    }
                }
            }
        }
    }

    public String getError() {
        return error.toString();
    }

    private void mergeFileRecursive(File source, BufferedOutputStream bufferedOutputStream) {
        if (source.isFile()) {
            try {
                String contents = FileUtils.readFileToString(source, Charset.defaultCharset());
                String start = "<advertise";
                int startIndex = contents.indexOf(start);
                if (startIndex >= 0) {
                    //bufferedOutputStream.write(("<!-- contents for " + source.getAbsolutePath() + " -->\r\n").getBytes("UTF-8"));
                    bufferedOutputStream.write((contents.substring(startIndex) + "\r\n").getBytes("UTF-8"));
                }
            } catch (IOException e) {
                error.append("Erro ao ler fileOutputStream: ");
                error.append(e.getLocalizedMessage());
                error.append("\n");
            }
        } else if (source.isDirectory()) {
            File[] files = source.listFiles();
            if (files != null) {
                for (File file : files) {
                    mergeFileRecursive(file, bufferedOutputStream);
                }
            }
        }
    }
}
