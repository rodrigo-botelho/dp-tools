package br.com.digitalpages.xmlxsdvalidator;

import br.com.digitalpages.xmlxsdvalidator.validator.XMLFolderValidator;
import br.com.digitalpages.xmlxsdvalidator.validator.XMLValidator;
import br.com.digitalpages.xmlxsdvalidator.validator.XMLXSDValidator;

import java.io.File;

/**
 * Created by rodrigo.botelho on 20/03/2017.
 */
public class Main {
    public static void main(String[] args) {
        if (args.length < 3) {
            String programName = new File(Main.class.getProtectionDomain()
                    .getCodeSource()
                    .getLocation()
                    .getPath())
                    .getName();
            System.out.println("Usage: " + programName + " xml-file|xml-dir xsd-file [xml-name-pattern]");
            return;
        }

        String xmlFileOrDir = args[0];
        String xsdFile = args[1];
        String xmlNamePattern = args.length > 2 ? args[2] : null;
        XMLXSDValidator validator;

        if (new File(xmlFileOrDir).isDirectory()) {
            validator = new XMLFolderValidator(xmlFileOrDir, xsdFile, xmlNamePattern);
        } else {
            validator = new XMLValidator(xmlFileOrDir, xsdFile);
        }

        validator.validate();
        String error = validator.getError();

        if (error != null && error.length() > 0) {
            System.out.println("Alguns erros:");
            System.out.println(error);
        } else {
            System.out.println("XML validados.");
        }
    }
}
