package br.com.digitalpages.xmlxsdvalidator.validator;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

/**
 * Created by rodrigo.botelho on 20/03/2017.
 */
public class XMLValidator implements XMLXSDValidator {
    private String xmlPath;
    private String xsdPath;
    private boolean valid;
    private StringBuilder error;

    public XMLValidator(String xmlPath, String xsdPath) {
        this.xmlPath = xmlPath;
        this.xsdPath = xsdPath;

        error = new StringBuilder();
    }

    public void validate() {
        error.setLength(0);
        valid = false;

        File schemaFile = new File(xsdPath);
// or File schemaFile = new File("/location/to/xsd") etc.
        Source xmlFile = new StreamSource(new File(xmlPath));
        SchemaFactory schemaFactory = SchemaFactory
                .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        try {
            Schema schema = schemaFactory.newSchema(schemaFile);
            Validator validator = schema.newValidator();
            validator.validate(xmlFile);
            valid = true;
        } catch (SAXException e) {
            error.append("Arquivo ");
            error.append(xmlFile.getSystemId());
            error.append(" não é válido.\r\n");
            error.append(e.getLocalizedMessage());
            error.append("\r\n");
        } catch (IOException e) {
            error.append("Arquivo ");
            error.append(xmlFile.getSystemId());
            error.append(" não é válido.\r\n");
            error.append(e.getLocalizedMessage());
            error.append("\r\n");
        }
    }

    public boolean isValid() {
        return valid;
    }

    public String getError() {
        return error.toString();
    }
}
