package br.com.digitalpages.xmlxsdvalidator.validator;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rodrigo.botelho on 20/03/2017.
 */
public class XMLFolderValidator implements XMLXSDValidator {
    private String xmlFolder;
    private String xsdPath;
    private String xmlNamePattern;
    private StringBuilder error;

    public XMLFolderValidator(String xmlFolder, String xsdPath, String xmlNamePattern) {
        this.xmlFolder = xmlFolder;
        this.xsdPath = xsdPath;
        this.xmlNamePattern = xmlNamePattern;

        error = new StringBuilder();
    }

    public void validate() {
        error.setLength(0);

        List<File> xmlFiles = new ArrayList<File>();
        appendXMLFilesRecursive(xmlFolder, xmlFiles);
        int errorCount = 0;

        XMLValidator xmlValidator;
        for (File file : xmlFiles) {
            xmlValidator = new XMLValidator(file.getAbsolutePath(), xsdPath);
            xmlValidator.validate();

            if (!xmlValidator.isValid()) {
                errorCount++;
                error.append(xmlValidator.getError());
                error.append("\r\n");
            }
        }

        if (errorCount > 0) {
            error.append("Total de erros: ");
            error.append(errorCount);
            error.append("\r\n");
        }
    }

    private void appendXMLFilesRecursive(String path, List<File> xmlFiles) {
        File pathFile = new File(path);
        String fileName;
        if (pathFile.isDirectory()) {
            File[] files = pathFile.listFiles();
            if (files != null) {
                for (File file : files) {
                    appendXMLFilesRecursive(file.getAbsolutePath(), xmlFiles);
                }
            }
        } else if (pathFile.isFile()) {
            fileName = pathFile.getName();
            if (xmlNamePattern != null) {
                if (fileName.equalsIgnoreCase(xmlNamePattern)) {
                    xmlFiles.add(pathFile);
                }
            } else if (fileName.toLowerCase().endsWith(".xml")) {
                xmlFiles.add(pathFile);
            }
        }
    }

    public boolean isValid() {
        return error.length() == 0;
    }

    public String getError() {
        return error.toString();
    }
}
