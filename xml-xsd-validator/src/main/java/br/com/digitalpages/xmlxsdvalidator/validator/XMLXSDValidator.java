package br.com.digitalpages.xmlxsdvalidator.validator;

/**
 * Created by rodrigo.botelho on 20/03/2017.
 */
public interface XMLXSDValidator {
    void validate();
    boolean isValid();
    String getError();
}
